// @ts-check

/**
 * 
 * @param {String} country - pais
 * @returns {String} - identificador de impuesto, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
export const getCountryTaxIdentifier = (country: String): String => {
    switch (country) {
        case 'AR':
        return 'CUIT';
        case 'CL':
        return 'RUT';
        case 'UY':
        return 'RUT';
        default:
        return 'Invalid';
    }
}

/**
 * 
 * @param {String} country - pais 
 * @returns {String} - Mascara de identificador de impuesto, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */

export const getCountryTaxIdentifierMask = (country: String): String => {
    switch (country) {
        case 'AR':
        return '[00]-[00000000]-[0]';
        case 'CL':
        return '[00].[000].[000]-[0]';
        case 'UY':
        return '[00].[000000].[000]-[0]';
        default:
        return 'Invalid';
    }
}

/**
 * 
 * @param {String} country - pais
 * @returns {Number|String} - Largo del identificador de impuesto, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */

export const getCountryTaxIdentifierLength = (country: String): Number|String => {
    switch (country) {
        case 'AR':
        return 11;
        case 'CL':
        return 9;
        case 'UY':
        return 12;
        default:
        return 'Invalid';
    }
}

/**
 * 
 * @param {String} country - pais
 * @returns {String[]|String} - Lista de formatos de patentes, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
export const getCountryPlateFormat = (country: String): String[]|String => {
    switch (country) {
        case 'AR':
        return ['AAA999', 'AA999AA'];
        case 'CL':
        return ['AA9999', 'AAAA99'];
        case 'UY':
        return ['AAA9999'];
        default:
        return 'Invalid';
    }
}
/**
 * 
 * @param {String} country - pais
 * @returns {String} - Nombre del DNI por pais, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
export const getCountryDocumentIdentifier = (country: String): String => {
    switch (country) {
        case 'AR':
        return 'DNI';
        case 'CL':
        return 'CI';
        case 'UY':
        return 'CI';
        default:
        return 'Invalid';
    }
}
/**
 * 
 * @param {String} country - pais
 * @returns {Number|String} - Cantidad de digitos del DNI, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
export const getCountryDocumentIdentifierLenght = (country: String): Number | String => {
    switch (country) {
        case 'AR':
        return 8;
        case 'CL':
        return 9;
        case 'UY':
        return 8;
        default:
        return 'Invalid';
    }
}

/**
 * 
 * @param {String} number - Numero del identificador de impuestos a formatear
 * @param {String} country - pais
 * @returns {String} - Numero formateado con - o . depende corresponda, devuelve 'Invalid' si el pais no existe o se envio mal el parametro
 */
export const formatCountryTaxIdentifier = (number: String, country: String): String => {
    switch (country) {
        case 'AR':
        return number.substring(0, 2) + '-' + number.substring(2, 10) + '-' + number.substring(10, 11);
        case 'CL':
        return number.substring(0, 2) + '.' + number.substring(2, 5) + '.' + number.substring(5, 8) + '-' + number.substring(8, 9);
        case 'UY':
        return number.substring(0, 2) + '.' + number.substring(2, 8) + '.' + number.substring(8, 11) + '-' + number.substring(11, 12);
        default:
        return 'Invalid';
    }
}

export default {
    getCountryTaxIdentifier,
    getCountryTaxIdentifierMask,
    getCountryTaxIdentifierLength,
    getCountryPlateFormat,
    getCountryDocumentIdentifier,
    getCountryDocumentIdentifierLenght,
    formatCountryTaxIdentifier
}